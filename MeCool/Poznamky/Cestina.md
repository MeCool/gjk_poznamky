# Fredyho hlášky
- Elektra o hodině celkem kecá, Fredy si toho všime
	> Elektro, vy jste nějak elektrizovaná, ne? ... Tyjo to se mi povedlo. Vy jste mě opravdu asi nabila.
- Fredy mluví o strašidelných barokních pohádkách
	> Juj, to se budem bát ve stanu!

- bez kontextu
	> Baroko je takové výbušné pššš...

- Fredy chce, aby někdo další hodinu představil Pastoriány.
	> Kdo by mohl přístí hodinu něco říct o Pastoriánech? Kláro? Ale až zítra....
	
    > Jo, já si to jen píšu.
	
    > No, Ťuky ťuk.

- Fredy řekne vtip o černé televizi
	> Tak teď jsem si užil svou jedinou minutu v roli baviče!

- Všichni furt trochu brblají (poslední listopadový den)
	> Tyjo to už jsou Vánoce? Si tady řikáte, co asi dostanete?

- Je pátek, ve středu byl ples, ve čtvrtek všichni jen přežívali. Venku už je týden zima a trochu sněhu.
	> A proč nejste vůbec na horách? No já to myslím vážně! Jste byli na plese, tak si dám volno a někam vyrazím. No teď už ne, to jste měli vyjet dřív....

- Je středa, první hodina. -> Ema chodí pravidelně pozdě. Přichází Matěj, 10 min pozdě.
	> A? Že by Ema? Ohh, pan Matěj.
	- po 40 klepe někdo na dveře. (Hedvika)
		> A, už i Ema přišla? Jooo, vždyť ona dneska nepřijde....

---

## Projekty

### Umělec a jeho texty
- max 20min
- souvislosti
- vybrat 2-3 texty
	- rozebrat

## Pojmy

### Impesionismus
- zachycení prchavého okamžiku "tady a teď"
- hodně citově založné
- autor se profiluje do emocí, které chce zachytit

24.10.2018

# Středověká literatura

- Většinou psáno latinsky
- Dokonce i scripta z medicíny se psaly ve verších (asi aby se to lépe učilo)

## Scholastika

- = Bernard z Clairvaux (1153+)
	- reformátor
	- zastánce realismu -> realismu, ?kde všechny věci doopravdy existují?
- diskuze, kde rozumem se mělo potvrdit, že to tak doopravdy je
- dokonce stanovili oponenta ( pro diskuze )
- => písemné přijímání dogmat

## Nominalismus

- = Abélard
	- -> Historie mých pohrom
	- teolog v Paříži, jeden z prvních nominalistů
 	- Nominalismus => pojmy existují pouze podle jména, ale rozum k nim musí blíž
	- "neexistuje lidstvo, existují lidé"
		- "rostlinstvo nezkoumám, zkoumám až tu rostlinu"
	- nejsou kacíři, jen věří, že rozum je síla a je potřeba ji využít

## Středověká vysoká škola

- Vznikají blízko kláštěrů, 4 fakutly
	- artisická (od 14 let)
	- medicína / práva (od 20 let)
	- teologie (od 35 let)
 - = John Scott
	- Františkán, mistr Oxfordu
	- zdůrazňoval roli vůle a možnost volby
 - = Vilém Occam
	- teorie jednotlivin => "není lidstvo, jsou lidé"
	- také z Oxfordu

### Žákovská produkce

- satira ve formě písniček, studenti si tak dělali z každého srandu
- studenti psali "nižší" literaturu, profesoři psali "vyšší"
-  = Primas
-  = Hugo Orelánský

---

31.10.2018

### Duchovní lyrika

- pana Marie, duchovní legendy, prostor pro lásku
- františkáni
	- = sv. Boneventura
		- povolil, že řád může mít majetek
	- = František z Assisi (12-13.stol)
		- hlavní osobností františkánů
		- snažili se reformovat církev, neútočili však na ni jako Hus
		- -> píseň na bratra Slunce (Slunce ve smyslu boha)
		- měl kamarádku


- = sv. Klára
	- zakladatelka klaristek (ženský klášter)
	- je též z Assisi

- dominikáni (1215)
	- Dominigo de Guzmán = zakladatel dominikánů
	- byla jim svěřena inkvizice
- kataři
	- sekta z jižní francie
	- křížová výprava proti katarům
- valdenští
	- = P. Valdo
	- dobrovolná chudoba, pokora,mír

---

1.11.2018

## Duchovní literatura
- mnoho žánrů souvisejících s vírou

---

2.11.2018

## Dvorská epika
- nejdříve písně o hrdinských činech
- -> courtoázní kultura
	- => dvorní kultura, odehrává se na dvorech - střední + vyšší společnost
	- rytíř, který je vzorem, chová se velice zdvořile k ženám
		- neustále je se ženami v kontaktu, dvoří se i vdaným ženám
		- za ženy i bojovali
### Rytířský román
- delí se na několik okruhů:

	- **antický**
		- zpracování antických příběhů = příběhy Alexandra velikého (Alexandreidy)
		= Dějiny dobití Tróje
		= Román o Thébách
	- **bretonský / keltský**
		- oblast severozápadní Francie, ústup před sasy, usazují se tam Britoni
		= Král Artuš a jeho rytíři u kulatého stolu
		= Mýtus o sv. Grálu
---

9.11.2018

## Dvorská lyrika/poezie
- Vrchol kulturního zážitku
- Téma o **lásce** v jakékoli podobě
- Původně byla *erotika* hodně RAW, postupně se začala anonymizovat
- Alba -> svítáníčko
	- loučení páru po společně strávené noci

---

21.11.2018

##  Měšťanská literatura

- vedle duchovní, rytířské, dvorské...
- tyto motivy padají do mešťanského prostředí
- z měst se stává silný subjekt, který se časem stane hlavním
- města mají své univerzity (žáci umějí psát, mluvit cizími jazyky), dále kláštery
-> přibývá lidí, kteří umějí číst a psát
- přibývá kupců, podnikatelů - mají menší smysl pro "duchovno", mají lepší vnímání pro reálný svět, chtějí se bavit
- měšťanská literatura se vyznačuje realismem, satirou, výsměchem

### Měšťanská epika

- patří do ní **fably** (= krátké epické veršované povídky sloužící k pobavení)

### Fably

- hrubý, drzý humor
- hlavními konzumenty byla střední třída a nevzdělaní
- terčem bývají doktoři, střední třda, menší dvory
- hlavními spisovateli byli žongléři, cirkusáci, komedianti, nižší vrstva
- mívá 18-400 veršů
- drsný, sirový, hrubý
- na konci mravní ponaučení
- recitováno na společenských akcích
- **opak dvorské literatury**

### Zvířecí román

- drsné a vulgární
- delší než bajka
- polidštěná zvířata
- objevuje se satira
- Francie, Německo, Rakousko i Česko
- **Renart**
	- "Román o Renartovi"
	- hlavní postava lišák
	- je lstivý, v krutém světě úspěšný
- **Yzengrin**
	- vlk, který je hloupý
	- lišák ho využívá
- **Tybert**
	- kocour

### Exemplum

- určitý vzor/příklad
- výchovný, krátký veršovaný/próza text

### Mandevil

- cestopisy
- některá fakta si sám vymýšlel

### Odoric z Pordenone

- 

## Městská lyrika

- = Román o růži
	- 2 části
	- **1** - (1240), výbuchovní - DeLorris, směřuje do zahrady lásky, kde středem je růže
		- žárlivost
	- **2** - (1270), DeMeung, delší - 18 000 veršů, hrdina dojde k růži a dosáhne tajemství růže
		- více pasuje do městského prostředí, více menších příběhů, satira, kritika
	- pomezí epiky a lyriky

- = Meier Helmbrecht
	- skladba o sedlákovi
	- parodie vyšších vrstev
	- chce se chovat jako rytíř

### Deník a jízda pana Lva z Rožmitálu

- Lev z Rožmitálu 
	- hlavní postava výpravy
- jeden z prvních cestopisů

## Gnómická literatura

- soubory krátkých útvarů s výchovným charakterem
- sbírka rčení, anekdot
- lyrika

- = Žertovník
	- 1000 veršů
	- 13. století záp. Ev (14.stol) Čechy

- Zdoronezdvořák / Antikameradus
	- -> *Frowin* - farář, Krakow
	- 500 veršů 
	- často kritizuje české mravy

- Hádankář
	- *Bartoloměj z Chlumce* / *Klaret*
		- profesor, učitel
	- poučuje hlavně studenty

## Písemnictví Velké Moravy

- Stát vznikající v 9.stol
- Převládá křesťanství
- Říše na území Moravy, Slovenska a Maďarska
	- Staré Město u Uherského Hradiště
	- Mikulčice 

### Rastislav + Cyril a Metoděj

- Pozval misionáře z východu -> Byzancie
- Michal III. ( Byzantský císař)
	- pošle dva bratry => **Konstantin**/Cyril a **Metoděj**
	- přicházejí z vyspělého státu
	- poslal svoje nejlepší muže
**Konstantin**/Cyril a **Metoděj**
- Byli posláni jako křesťanská misie
- Politicko-organizační cíl
	- chtěli podpořit funkčnost státu
	- přišli tedy taky jako politici
- **Kulturně-výchovná činnost**
	- **Konstantin** je velmi velkou osobností 
	- **Metoděj** byl organizační pragmatik 
Z jihu chtějí přinést jazyk, který by jim pomohl.
=> Přinesou **staroslověnština**
	- Kulturní jazyk
=> Přinesou **hlaholici**
	- pochází z malé řecké ?...?
Latině zde nikdo nerozumněl, chtěli proto přinést srozumitelný a jednodušší jazyk.
Chtěli, aby jim bylo rozumnět. Dali proto staroslověnštinu do mše. A to nikdo předtím neudělal.
	- Mše mohly být pouze latinsky
	- To se nelíbilo, hlavně církvi

- Přeložili **evangelium** z řečtiny
- právo šířit evangelium, ducha, cirkev svým jazykem
- -> personifikace, anafora, aliterace, zvukomalba, volný verš
	- anafora = opakování určitých slov, například na vypíchnutí
	- aliterace = opakování hlásek z nějakého důvodu (vítr šustil -> budu používat hodně "s")

- vznikne sbírka žalů => Žaltář
- Metoděj nejspíš přeložil i starý zákon
- Připravovali liturgické spisy (pro mši)
- překládá modlitby
- vznik velkomoravské literární školy
- 1033 - Sázavský klášter - zákoník -> Nomokánon

**Legendy a modlitby**
	- psáno latinsky a staroslověnsky

### Život Konstantinův a Život Metodějův 

- dvě původní legendy
- Konstantinův má větší hodnotu než Metodějův
- Psáno ve vysokém stylu, veršováno
- smysl obhájit našich bratří před duchovními nepřáteli
- analyzována jejich mise
- 1/2 textu jsou rozhovory
	- méně děje, disputace
	- hlavně disputuje staroslověnský jazyk + křesťanství
- psáno v próze


# Konec středověku

## Jazyková složka

- jazyk **latinský**
	- stále byl velice rozšířený

- **italština**

- **němčina**
	- horníci, ekonomové
	- trochu vyspělejší skupina
	- často byli bohatší

### Laicizacce

- nedostudovaný amatér, student
- vzdělaní příslišníci šlechtických rodů
- nová, nezvyklá témata
	- rytířská, dvorská lyrika
	- městská poezie
- -> proces nových autorů, témat, žánrů

- = Pasionál abatyše Kunhůty
	- sbírka legend o mučednících

- = Brevíř velmistra Lva
	- soubor modliteb určené k modlení v určité částí dne, týdne, roku

- = Legenda Aurea
	- latinský soubor s legendami z celé Evropy

## Duchovní lyrika a epika

- = Spor duše s tělem
	- žánr sporu je velmi oblibený

- = Apokryf o Jidášovi
	- apokryf/legenda
	- autor spojil tradiční příběh o **Jidášovi** s příběhem o králi **Oidipovi**

- **Sv. Jiří**
	- zabíjí draka, osvobodí princeznu
	- vržen do vězení, neboť není pohan
		- tam zázračně přežije pokusy o popravu (uvařen, zakopán)

## Dalimilova kronika

- končí roku 1814
- je prostý člověk, má jednoduchou úvahovou rovinu
- zná však hodně o politické situaci atd.	(ví mnoho o to, co se ho *týká*)
- psáno v lyrice
- autor pocházel ze staré české šlechty
	- nižší šlechta
	- kritizoval tedy tu nejvyšší
- kritický vůči němcům
	- kritický vlastně ke všem
	- nejhorší byla odlišnost ve víře
	- další pak rozdíly mezi společenskými stavy
	- **německá náplava** zde totiž získává moc

## Baroko

- obecně **17.století** je doba baroka
	- postupně šlo ze západu víc do evropy
	- u nás je barokní vrchol na začátku 18.stol

- = John Nilton
	- Ztracený ráj
- = J.A.Komenský
	- Labyrint světa a ráj srdce
- = Torquato Tasso

- **Barokní eposy** (epika)
	- střety hodnot
		- protiklady (moc, barvy, vztahy)
		- hrdina si volí těžší cestu
			- protože jenom ta těžší je vítěžná

- **Lyrika** 

- duchovní milostná *erotika*
	- všichni už jsou křesťani, ale něktěří stále hřeší
	- (nachází se i v epice)

- **Metafyzická poezie**
	- anglický manýrismus 
		- J. Donne
		- nadfyzický svět, který však ovlivňuje ten náš


## 14. století

- k vládě se dostává Jan Lucemburský
- nastupuje po něm Karel IV.

## Duchovní lyrika

- psáno spíše latinsky
- většinou modlitby
- nové jsou *plankty*
	- pláče / nářky panny Marie, když umírá Ježíš
- vznikají některé legendy
	- = Život sv. Kateřiny
	- = Legenda o sv. Prokopu

## Dvorská lyrika

- = **Závišova píseň**

## Dvorská epika

- = **Tristram a Izalda**
	- podložena 3 německými texty
	- psáno veršovaně

## Lyrika satirická

- = **Píseň veselé chudiny**


## Poezie

- = **Hradecký rukopis**
	- soubor lyrických skladeb
	- neznámý autor

- = **Desatero božích přikázání**
	- rozdělena na 10 částí (...)
	- použití dialogu
	- jednoduchá charakteristika postav

- = **Satiry o řemeslnících a konšelích**
	- píše o praktikách ostatních
	- vyhrožuje jim většinou peklem

## Próza

- = Oráč z Čech
- = Tkadleček
- = Vita Caroli
- = **Exemplum**
	- příkladem ukazuje nějaký problém
	- krátký text

- = **Podkoní a žák**
	- 490 veršů
	- spor mezi zákem a podkoním
		- oba dva se chtějí chválit

## Carlo Goldoni

- pochází z Benátek
- libreto
- melodrama
	- jednoduchý děj, jednoduchá myšlenka, nemění se postavy
- inspiroval se postavami z Del'a Art
	- upevnil scénář, použil jako hrdiny měšťany
	
**Poprask na laguně** (1762)

- 3 dějství
- řeší mezilidské vztahy
- komedie (trocha vážnosti díky vztahům)
- psáno hovorovou formou
- postavy přehledné, moc se nemění

## Jan Hus
**O šesti církevních omylech**
- člověk se má řídit hlavně tím, co říká Bůh v bibli
	- Bible => hlavní zdroj pravdy
- podle Husa se církev dopouští omylů, když se nedrží bible

**Postyla**
- sepsané kázání, lehce odbornější
	- **Jak vypadá kázání?**
		- nejdříve se přečtou určité verše
		- pak upozorní na podobenství a zopakuje/zvýrazní výchovný aspekt

**Dcerka**
- radí ženám, aby se chovali vůči katolíkům nenapadnutelně
	- aby nechodila nemravně
	- aby si nemohli katolíci rejpnout

**O pravopisu českém**
- pravděpodobně zavedl háčky + čárky


- literatura výrazně degraduje


### Jakoubek ze Stříbra

- uni. mistr
- **Jistevnický kancionál**
	- převládají časové písně
- **Hádání Prahy s Kutnou Horou**
	- Praha -- *alegorie* --> s Husitsvím
		- hezká žena
	- Kutná Hora -- *alegorie* --> s katolíkama
		- nepohledná žena
	- Ježíš dá za pravdu Praze

### Vavřinec z Březové

**Píseň o vítězství u Domažlic**

**Václav, Havel a Tábor**
- protihusitské dílo
- vedou spor
	- Tábor všechno níčí
	- Havel se nemůže rozhodnout
	- Václav z toho vychází nejlíp

## Pedro Calderon de la Barca

**Život je sen**
- smysl života a co je život
- sen je v životě stěžejní
	- furt má ale smysl žít i ten *reálný*(??)
- => dlouhé monology

### Schopenhauer
- velký pesimista
- **Svět jako vůle a představa**
	- svět je bez boha, bůh zemřel
	- podstatou je vůle/ideejedince
	- cení umění a **hudbu** (prostor našich ideí)
	- svět považuje za nejhorší z možných (utrpení, zoufalství)
- **Životní moudrost**
	- kniha o životě, empatie

### Nietzsche (1900)
- filosof, básník
- demokracie je vláda slabých, odmítá ji
- **Zrození tragédie z ducha hudby**
	- Apollinský princip (žít v řádu)
	- Dionýský princip (agresivita, extrémní žití)
- **Mimo dobro a zlo**
	- nadčlověk
		- situace/možnost, kdy člověk může zastoupit Boha
- **Věčný návrat**
	- život je určitý kruh
		- když jsme jednou dole, tak se dostaneme někdy na vrchol

### Zaratmuštra
- Obraz mudrce, který přichází mezi lidi a *jako ježíš* se snaží poučit ostatní lid, který potká po své cestě z hor
	- potkává zajímavé osobnosti, pak i své 'žáky'

### H. Bergson (1941)
- Odmítá rozum, racionální aktivity
- **Vývoj tvořivý**

### P. Verlaine (1896)
- byl to lyrický básník, více dekadent
- **Saturnské básně**
	- krajinářská poezie, 'krajina ducha'
	- tesklivé, smutné
- **Písně beze slov** 

### S. Mallarmé
- **Vrh kostek nikdy nevyloučí náhodu**

### E. Verhaeren
- pracuje s mýty a pověstmi
- **Večery**, **Černé pochodně**, **Celé Flandry**

### M. Maeterlinck
- symbolická dramata
- **Modrý pták**
- **Slepci**

### H. Ibsen
- **Divoká kachna**
	- symbol pozůstatku divočiny (kachna)
	- Hedvika zabila kachnu, aby ukázala, že dokáže něco obětovat

