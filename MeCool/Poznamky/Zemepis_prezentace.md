---
geometry: margin=2cm
papersize: a4
fontfamily: times
---

# Prezentace - Složení obyvatelstva


# **AMERIKA**

## Střední Amerika

- Etnické složení
	- mesticové (66%)
	- domorodí Indiáni (16,5%)
	- běloši (14,5%)
	- černoši a mulanti (2%)
	- ostatní (1%)

- Jazyky
	- Španělština -> na většině území
	- Angličtina -> na některých pobřežích
	- Lenca, Pipil a Majské dialekty

- Náboženství
	- díky kolonizaci v 16.století => nejrozšířenější křesťanství
	- 1960 - vzestup protestanství

- Věková pyramida
	- Už dvacet let je přibližně 9% novorozených 

## Mexiko

- Etnické složení
	- Mestizo (54%)
	- Mexican white (15%)
	- detribalized Amerindian (10,5%)
	- other Amerindian (7,5%)
	- others (1%)

- Jazyky
	- Španělština (hlavní jazyk?)
	- + 63 domorodých jazyků
 
- Náboženství
	- Římskokatolické (82%)
	- jiné křeťanství (17,8%)
	- ostatní náboženství(0,2%)

## USA

- velmi nízká hustota osídlení

- Etnické složení
	- většinou bílé 
	- velké "%" hispánců (španělská kultura)

- Náboženství
	- hodně křesťanské, mnoho odvětví
	- velká ortodoxní menšina

- Jazyky
	- Angličtina
	- Španělština (hispánci)
	- pak menšiny (čínština, polština, francouzština)

## Kanada

- Etnické složení
	- Kanadská
	- Anglická
	- Franzouská
	- (Skotská)
	- první národy (mongolové)

- Jazyky
	- Angličtina
	- Francouzština
	- MishMash

- Náboženství 
	- Křesťani (50%)
	- Katolíci (50%)

## Peru

- Etnické složení
	- Amerindian (45%)
	- Mestizo (37%)
	- Bílí (15%)
	- ostatní (3%)

- Jazyky
	- Španělština (84%)
	- Quechua (13%)
	- ostatní (3%)

- Náboženství
	- Římsko-katolická církev (81%)
	- Evangelici (13%)
	- ostatní (6%)

- Věková pyramida
	- více žen
	- regresivní typ

## Chile

- Etnické složení 
	- mnoho kmenů

- Jazyky
	- hlavně španělština + angličtina

- Náboženství
	- Římsko-katolická církev

## Bolívie

- Etnické složení
	- Amerindian (30%)
	- Quechua (30%)

- Jazyky 
	- Španělština
	- Quechuanština

- Věková pyramida
	- progresivní
	- začíná se stabilizovat až poslední dobou (stacionární)

## Brazílie

- Etnické složení
	- Bílí (48%)
	- Mulati (43%)
	- Černí (8%)
	- žlutí (1%)

- Náboženství
	- Katolíci (65%)
	- Protestanti (22%)
	- spiritismus (2%)

- Jazyky
	- Portugalština
	
- Věková pyramida
	- snižování plodnost
	- 45% lidí je v produktivním věku

## Argentina
	
- Etnické složení
	- Bílí a mestici (97%)
	- původní obyvatelé (2,4%)

- Náboženství 
	- Římsko-katolická (92%)
	- Protestanti (2%)
	- Židé (2%)

- Jazyky
	- Španělština

## Paraguay
	
- Etnické složení
	- mesticové (95%)

- Náboženství
	- Římsko-katolická (90%)
	- Protestanti (6%)

- Jazyky
	- Španělština

## Uruguay

- Etnické složení
	- bílí (88%)
	- černí (5%)
	- domorodci (2%)

- Náboženství
	- Římsko-katolické (47%)
	- nekatoličtí křesťané (11%)

## Ekvádor, Kolumbie, Venezuela

- Etnické složení
	- Mestic
	- Afro-Kolumbijci
	- Indiáni
- Náboženství
	- Katolická církev
	- Protestanská církev
	- Atheisté
- Jazyky
	- Španělština (úřední jazyk)
	- indianské jazyky

## Guyana

Původně Britská kolonie - proto angličtina atd.

- Etnické složení
	- Východní indové (40%)
	- Afroameričané (30%)
	- Smíšené (20%)

- Jazyky
	- Angličtina (úřední jazyk)
	- Guinejská kreolština

- Náboženství
	- Křesťané (50%)
	- Hinduisti (30%)
	- Muslimové (10%)

# **EVROPA**

## Severní Evropa

Vysoká naděje dožití, vysoká životní úroveň

- Jazyky
	- Dánština
	- Finština
	- Islandština
	- Norština
	- Švédština

- Náboženství
	- Převážně protestanství
	- zvýšený počet vyznavatelů islámu

- Etnické složení
	- Finové
	- Dánové
	- atd.

## Západní Evropa

Velká Británie, Německo, Irsko, Benelux

- Etnické složneí
	- běloši (90%)
	- Asiati + černoši (10%)
	- Ve **GB** hodně Indů a Pákistánců
	- V **GER** hodně Turků a Syřanů
	- V **BENELUX** Maročani a Portugalci, kteří pracovali v dolech

- Jazyky
	- Angličtina (**GB**)
	- Němčina (**GER**)
	- francouzština, nizozemština, vlámština, němčina (**BENELUX**)

- Náboženství
	- Katolíci (70%)
	- Mulsimové (10%)
	- Ateisté (20%

## Jižní Evropa

- Náboženství
	- Římskokatolické, Pravoslavné

- Etnické skupiny
	- Na pyrenejském poloostrově hispánci (dovezeni Španěly a Portugalci z Jižní Ameriky)

## Rusko & Kazachstán

### Rusko

- Etnické skupiny
	- Hlavně Rusové, dále Tataři, Ukrajinci, Baškirkové aj.

- Jazyky a písmo
	- oficiální jazyk ruština
	- azbuka

- Náboženství
	- pravoslavní; duchovní, kteří se ale nepřiklánějí ke konkrétní víře; ateisté

### Kazachstán

- Etnické skupiny
	- Hlavně Kazaši, dále Rsové, Uzbeci, Ukrajinci, Tataři aj.

- Jazyky a pásmo
	- oficiální jazyk kazaština
	- azbuka (chtějí změnit na latinku)

- Náboženství
	- 70% sunnitský islám; 30% pravoslavní; evangelíci

## Arménie & Gruzie & Azerbajdžán

### Arménie

- Etnické složení
	- 98% Arméni, zbytek Kurdové a Rusové

- Náboženství
	- arménská apoštolská církev 93%
	- islám 4%
	- ostatní 3%
	
- Jazyky a písmo
	- oficiální jazyk arménština

### Gruzie

- Etnické složení
	- Gruzíni 84%
	- Azerové 7%
	- Rusové 2%
	- ostatní 7%
	
- Náboženství
	- pravoslaví 84%
	- islám 10%
	- arménské apoštolství 4%
	- ostatní 2%

- Jazyky
	- gruzínština
	- abcházština
	- ruština
	
### Azerbajdžán

- Etnické složení
	- Azerové 92%
	- Lezgové 2%
	- Rusové, Arméni, Talyšové 6%

- Náboženství
	- muslimové 99,7%
	- malinko křesťanů

- Jazyky
	- 99,7% Azerbajdžánština
	- Ruština + jiné
	
## Jihovýchodní Evropa

- Etnické složení
	- europoidní rasa

- Jazyky
	- maďarsko - maďarština
	- rumunsko - rumunština
	- -||-

- Náboženství
	- Pravoslavní (70%)
	- Katolíci (15%)
	- Muslimové (10%)
	- ostatní (5%)

### Arabský poloostrov

- Populace
	- rychlý vzestup obyvatel
	- vysoká porodnost
	- hlavně muži, místy až 75%

- Etnické složení
	- Europodidní

- Spojené Arabské Emiráty
	- pouze 20% původních obyvatel
	- všichni přistěhovalci přišli hlavně kvůli byznysu

- Náboženství
	- kolébka Islámu

## Jihovýchodní Asie

- Obyvatelstvo
	- Thajci

- Jazyky
	- Thajsko - thajština
	- Malajsie - Malajština
	- -||-

- Náboženství
	- hlavně Budhismus (vznikl tam)
	- Islám (dovezen obchodníky z Indie)

### Filipíny

- Náboženství
	- 80% věřících
	- 90% křesťané
	- 5% muslimové

- Jazyky
	- španělština
	- italština
	- arabština
	- čínština

- Etnické složení
	- Igoroti (původní obyvatelé)
	- migrace Američanů
	- kolonizace Španělů

## Čína

Čína = Čínská lidová republika
Čínská republika = Taiwan

- Etnické složení
  - chanové (95%), čuangové, mandžuové...

- Jazyky
  - čínština & mongolština (vice versa)
  - regionální dialekty, písmo všude stejné
  - písmo v monoglsku cyrilice

- Náboženství
  - atheisté (Čína 50%)

## Libanon, Jordánsko, Sýrie, Izrael

- Etnické složení
  - Jordánci, Syřané, Kurdové, Libanonci, Arméni; Izrael - Židé

- Jazyky
  - hebrejšitna, arabština...

- Náboženství
  - křesťané, sunnité, šíté, židé

## Irák vs Írán

### Irák

- Islám (97%)
- Křestantsví (3%)

### Iran

- Muslimové (98%)
- Judaismus
- Křesťanství
- Zoroastrismus

## Afghánistán

- Paštunové (45%)
- Tádžikové (27%)
- Hazárové (8%)
- Uzbekové (9%)

- 30 jazyků, **paštú a dárí** oficiální

## Pákistán

- vznikl někdy v 50.letech
- Pandžábci (45%)
- Puštunové (15%)

## Indie, Nepál, Bhútán

- indoarijci (europoidní, indoevropané)
- nepálci + indo-nepálci (mongoloidní)
- bhůtové + nepálci (mongoloidní)

- hinduismus + muslimové
- hinduismus + buddhismus
- buddhismus + hinduismus

- nepálština + různé dialekty
- hindština + vedlejší angličtina
- bhútánština + nepálština + dialekty

## Japonsko + S. Korea + Jižní Korea

- 98,5% Japonci + Korejci + Číňané
- 98% Severokorejci + čínská menšina
- 99% Jihokorejci + čínská menšina

- 67% Sintosimus + 29% budhismus + 4% křesťanství 
- 64% bez vyznání + všechno ostatní je relevantní
- 49% bez vyznání + 26% buddhismus + 23% křesťanství

- japonština -> kandži, hiragana, katana
- korejština -> hangul
- korejština -> hangul

## Austrálie + Nový Zéland

- převážně běloši evropského původu
	- 75% je Britů
	- běloši (74%)
	- polynéští Maorové - původní (15%)
- velký vliv angličtiny
	- "národní" jazyk
- převládá křesťanství

## Oceánie

- +20 000 tichomořských ostrovů

### Polynésie

- mnoho nevelkých ostrovů
- polynésané
	- pochází původně z Taiwanu
- angličtina + francouzština

### Melanásie

- většina křesťané
- melanésané
- melanéské jazyky

### Mikronésie

- tisíce malých ostrovů
- domorodé a kolonizační jazyky
- mikronésané
- až 90% křesťanů

## Severní Afrika

- nejvíce Arabové + Berbeři + Niloté
- islám + islámské sekty + křesťanské církve + židé
- vetšinou arabština + občas Francouzština + ještě víc občas Tuaregština

## Západní Afrika

- Gambie, Guinea, Nigérie

### Etnické složení

- Hausové -> muslimové, hauština
- Jorubové -> muslimové / křesťané
- Mandingové -> muslimové
- Pygmejové -> nejmenší národ (max 150 cm)

### Jazyky

- Kvaské jazyky (jorubština, ibština)
- Čadské jazyky (hauština)

## Východní Afrika

- = Etiopie, Keňa, Tanzánie
- převážně zemědělské státy
- nejchudší na světě

### Etnické složení

- mulati (bílej + negr)
- negři

### Jazyky

- Bautuské jazyky [Nigerokonžská rodina]
	- svahilština, gaudština
- Kušitské a Semitské jazyky [Semito-hamitská rodina]
	- Somálština, oromština

### Náboženství

- Křesťanství - 60%
- Islám - 20%
- jiné - 20%

## Jižní Afrika

- =Botswana, Mozambik, Zimbabwe
- původní kmeny -> hotentoti
- Hamité, Kojsanové, Bantuové
- 27% **bílých**, převážně pak **negři**
- jazyky dle kolonizátorů
	- Nambie -> Afrikánština
