## Pokožkové deriváty

- určitá tělesa přeměněná z pokožky 
 - rohy
 - chlupy
 - peří

# Kosterní soustava

## Opora těla

| **HYDROSTATICKÁ kostra**                   | **VNĚJŠÍ kostra**                           | **VNITŘNÍ kostra**                                  |
|--------------------------------------------|---------------------------------------------|-----------------------------------------------------|
| uvnitř je tekutina, která není ztlačitelná | exoskelet, stále roste, přidávají se vrstvy | endoskelet - porifera, chordata (chrupavky a kosti) |
| většina žahavců (žížala)                   | měkkýši                                     | paryby, obratlovci                                  |
| výhodná u vodních organismů                | dokonalá ochrana celého těla                | roste s tělem                                       |
| nevhodné pro pohyb na souši                | neroste se živočichem                       | slabší krycí schopnosti                             |
| --                                         | energeticky náročné                         | --                                                  |
| --                                         | při svlékání zranitelný                     | --                                                  |

# Obratlovic

- všichni mají uzavřený systém - kardiovaskulární systém
- obecně mají srdce tvořené	
	- 1 či 2 předsíně
	- 2 či 2 síně
- třemi hlavními typy cév jsou
	- tepny
	- žíly
	- kapiláry
- jejich totální délka v lidkésm těle je 100 000km
- arterie se větví v menší arterioly a vedou ktev směrem **od srdce** pryč

**Tepna** -> od srdce (okysličená či neokysličená)
**Žíla** -> do srdce (neokysličená)

## Ryby

- srdce má jednu předsiň a jednu komoru

## Obojživelníci 

- srdce má tři prostory
	- dvě předsíně a jednu komoru
- existují dva oběhy 
	- plicní
	- systematický (tělní)
- k určitému míchání krve s kyslíkem a bez kyslíáku dochází v jediné komoře

## Plazi

- srdce má tři prostory
	- dvě předsíně a jednu komoru
- dvojí oběh
- komora je sice jen jedna, ale přepážka ji téměř zcela odděluje

## Ptáci a savci

- dvě předsíně a dvě komory
- zcela oddělen plicní a systematický oběh
-
# Vylučovací soustava

- udržuje v tkáních rovnováhu mezi obsahem solí a obsahem vody
- odvádí odpadní dusík
	- => produkt metabolismu bílkovn

| Amoniak              | Močovina                   | Kys. Močová                            |
|----------------------|----------------------------|----------------------------------------|
| 1g N -> 500ml H20    | 1g N -> 50ml H20           | 1g N -> 10ml H20                       |
| energeticky *laciný* | energeticky náročnější     | energeticky velmi *drahé*              |
| rozpustný ve vodě    |                            | relativně nerozpustné ve vodě          |
| mořské ryby          | savci, ryby, obojživelníci | ptáci, suchozemsští plazi a plži, hmyz |

## Protonefridie

- vylučuje absolutně všechno
- funguje na pricnipu bičíku a podtlaku

## Metanefridie

- cesta tekutiny je delší
	- dokáže tak využít většinu látek
	- nejsou takové ztráty
	- dokáží ji zahustit

## Ledivny

- filtrují krev
	- Bowmannův váček
		- místo, kde probíhá filtrace krve skrze vlásečnice
- kdyby v těle zůstaly dusíkaté látky, které jsou toxické, tak by později člověk umřel
- **Nefron**


# Hormonová soustava

## Hormony u bezobratlých

První u nezmara. Hormon podporuje růst a pučení a zabraňuje sexuálnímu rozmnožování
Feromon = hromon přenášející informaci z organismu do organismu
Hypotalamus
Hypofýza
Epifýza
Štítná žláza
Příštitná tělíska
Brzlík
Nadledvinky = tvoří se tam v malém množství i hormony opačného pohlaví
Inzulin = ve slinivce břišní
Slinivka břišní (Langerhansovy ostrůvky)
Vaječníky
Varlata

# Smyslová soustava

- Exteroreceptory = vnějšek těla
- Interoreceptory = vnitřek těla

- Fotoreceptory = světlo je jeho podnětem
- Mechanoreceptory = zaznamenává změnu tlaku, bolest, napětí a natažení
- Chemoreceptory = vnímání přítomností a množství molekul
- Nociceptory = napětí ve svalech šlachách
- Termoreceptory = změna teploty
- Proprioreceptory = vnímání pohybu a polohy částí těla

## Termoreceptory

- smyslový receptor umožňující vnímat teplo a chlad
- na změnu teploty reagují změnou chování
- Chlad => husí kůže, třes
- Teplo => pocení

## Mechanoreceptor

- vnímá tah, bolest, tlak
- vnímáme celým povrchem těla
- nejen povrch těla, také dutina ustní atd
- nejcitlivější konečky prstů, jazyk

# Rozmnožovací soustava

- zajištění předání genetické informace
- pohlavní / nepohlavní rozmn.
- hermafrodit /
- mimotělní spojení buněk (ryby)

## Pohlavní / nepohlavní

### Nepohlavní
- princip dělení buněk
	- výstupem je klon "rodičovské" buňky
- gonochorismus -> pohlavní dimorfismus
- 2x více potomků oproti pohlavnímu
- = štěpení			=> 1 celý --> dělí na 2 stejně velké
- = pučení			=> 1 celý ++> 2. vyrůstá z těla
- = fragmentace	=> 1 celý -+-> rozpadne se na několik kousků

### Pohlavní 
- splynutí dvou haploidních gamet dá vzniku zygotě
- sourozenci se od sebe geneticky odlišují
- hermafroditismus
- tasemnice si sama oplodní samičími buňkami samčí
	- v cizím prostředí by totiž nenašla další tasemnici
- 1/2 potomků oproti nepohlavnímu
