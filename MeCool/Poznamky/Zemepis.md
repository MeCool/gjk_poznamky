# Hospodářství

## Zemědělství

 - jedna z nejstarších hospodářských aktivit člověka
 - produkční / mimoprodukční (krajinotvorná)
 - 1/3 plochy Země
 - vyspělý země - vysoká produktivita
 - rozvojový země - samozásobitelský režim
 
 **Samozásobitelské**
 - zabezpečení základních potřeb
 - primitivní rotační, kočovné
 
 **Tržní**
 - chov pro obchod
 - moderní techn.
 	- **plantážnictví**
		- hlavně v rozvojových zemích
		- intenzivně komerční
		- vysoká specializace
