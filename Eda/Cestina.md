### "Béowulf"
- autor neznámý
- osaná staro-angličtinou, mytologické dílo
- příběhy 5. století, psáno 8.-10. století
  - Beowulf je bojovník, bojuje proti lidem i drakům -> v té bitvě umírá
  - odehrává se v Dánsku, Švédsku -> bažiny
  - rytíři dodržují hodnoty, mají svůj kodex
- hrdinská epika, veršovaný epos
- aliterace = začátky veršů jsou stejné
- variace = charakteristika hrdiny různými způsoby
- formulace = skupiny slov vyjadřující stejnou věci
  - kenningy:
    - cesta velryb (= moře)
    - kosti země (= balvany)
    - vlk včel (= medvěd (Beowulf))

## Církevní otcové
- vzdělanci, opati
- Sv. Augustinus, Řehoř Veliký, Jeroným
- snažili se o renezanci antiky

## Hrdinská epika
- __legendy__
  - o někom, kdo je mrtvý
  - o světci (Sv. Václav...)
  - začíná narozením -> život, co dokázal zázračného, mučednická smrt, jeho duch se vrací, učí, přináší poselství
  - "Legenda aurea"
    - soubor legend z celé Evropy
  - "Marty rologium"
    - soubor legend o mučednících
- __dějiny klášterů__
  - "Zbraslavská kronika"
  - _města_ se zajímají o dějiny -> městské kroniky (ekonomie, právo, kdy bylo co postaveno...)
- __dějiny křížových výprav__
  - _anály_
    - záznamy událostí, jak šli za sebou
  - _kronika_
    - přibarvovaly děj, měly by být věcné (nebyly)
    - __Beda Cihodný__
      - "Církevní dějiny Anglů"
    - __Kosmas__
      - "Kronika česká"
        - latinsky, próza


24.10.2018

# Středověká literatura

- Většinou psáno latinsky
- Dokonce i scripta z medicíny se psaly ve verších (asi aby se to lépe učilo)

## Scholastika

- = Bernard z Clairvaux (1153+)
	- reformátor
	- zastánce realismu
	- veří, že společné pojmy se staví před konkrétní věci (lidstvo > člověk...)
- diskuze, kde rozumem se mělo potvrdit, že to tak doopravdy je
- dokonce stanovili oponenta ( pro diskuze )
- => písemné přijímání dogmat

## Nominalismus

- = Abélard
	- -> Historie mých pohrom
	- teolog v Paříži, jeden z prvních nominalistů
 	- Nominalismus => pojmy existují pouze podle jména, ale rozum k nim musí blíž
		- "neexistuje lidstvo, existují lidé"
		- "rostlinstvo nezkoumám, zkoumám až tu rostlinu"
	- nejsou kacíři, jen věří, že rozum je síla a je potřeba ji využít

## Středověká vysoká škola

- Vznikají blízko kláštěrů, 4 fakutly
	- artisická (od 14 let)
	- medicína / práva (od 20 let)
	- teologie (od 35 let)
 - = John Scott
	- Františkán, mistr Oxfordu
	- zdůrazňoval roli vůle a možnost volby
 - = Vilém Occam
	- teorie jednotlivin => "není lidstvo, jsou lidé"
	- také z Oxfordu

### Žákovská produkce

- satira ve formě písniček, studenti si tak dělali z každého srandu
- studenti psali "nižší" literaturu, profesoři psali "vyšší"
-  = Primas
-  = Hugo Orelánský

---

31.10.2018

### Duchovní lyrika

- pana Marie, duchovní legendy, prostor pro lásku
- františkáni
	- = sv. Boneventura
		- povolil, že řád může mít majetek
	- = František z Assisi (12-13.stol)
		- hlavní osobností františkánů
		- snažili se reformovat církev, neútočili však na ni jako Hus
		- -> píseň na bratra Slunce (Slunce ve smyslu boha)
		- měl kamarádku


- = sv. Klára
	- zakladatelka klaristek (ženský klášter)
	- je též z Assisi

- dominikáni (1215)
	- Dominigo de Guzmán = zakladatel dominikánů
	- byla jim svěřena inkvizice
- kataři
	- sekta z jižní francie
	- křížová výprava proti katarům
- valdenští
	- = P. Valdo
	- dobrovolná chudoba, pokora,mír

---

1.11.2018

## Duchovní literatura
- mnoho žánrů souvisejících s vírou

---

2.11.2018

## Dvorská epika
- nejdříve písně o hrdinských činech
- -> courtoázní kultura
	- => dvorní kultura, odehrává se na dvorech - střední + vyšší společnost
	- rytíř, který je vzorem, chová se velice zdvořile k ženám
		- neustále je se ženami v kontaktu, dvoří se i vdaným ženám
		- za ženy i bojovali

- ### Tristan a Isolda
  - dvorská keltská epika, próza
  - motivy z celého světa
  - zpracováno nespočtem autorů
  - **příběh** :
    - odehrává se v Anglii
    - narodí se syn Trista, nemá rodiče -> Tristan neví, že je z královské rodiny, zjistí to, zabije draka, získá Isoldu, vypijí nápoj lásky, zamilují se, Tristan vydal Isoldu králi Markovi, Tristan uteče s Isoldou, společně zemřou

### Rytířský román
- delí se na několik okruhů:

	- **antický**
		- zpracování antických příběhů = příběhy Alexandra velikého (Alexandreidy)
		= Dějiny dobití Tróje
		= Román o Thébách
	- **bretonský / keltský**
		- oblast severozápadní Francie, ústup před sasy, usazují se tam Britoni
		= Král Artuš a jeho rytíři u kulatého stolu
		= Mýtus o sv. Grálu
---

9.11.2018

## Dvorská lyrika/poezie
- Vrchol kulturního zážitku
- Téma o **lásce** v jakékoli podobě
- Původně byla *erotika* hodně RAW, postupně se začala anonymizovat
- Alba -> svítáníčko
	- loučení páru po společně strávené noci

---

21.11.2018

#  Měšťanská literatura

- vedle duchovní, rytířské, dvorské...
- tyto motivy padají do mešťanského prostředí
- z měst se stává silný subjekt, který se časem stane hlavním
- města mají své univerzity (žáci umějí psát, mluvit cizími jazyky), dále kláštery
-> přibývá lidí, kteří umějí číst a psát
- přibývá kupců, podnikatelů - mají menší smysl pro "duchovno", mají lepší vnímání pro reálný svět, chtějí se bavit
- měšťanská literatura se vyznačuje realismem, satirou, výsměchem

## Měšťanská epika

 - patří do ní **fably** (= krátké epické veršované povídky sloužící k pobavení)

---

22.11.2018

## Fably

- 12. až 14. století
- šířili je studenti, profesionální zpěváci, cirkusáci
- literální epický útvar, 8 slabičný verš
- krátké (průměrně 400 veršů), bez poetického stylu
- přichází s poučením, autor nepřichází s pohledem k církvi, zaměřuje se na poučení v reálném životě
- realné, ovšem s menšími magickými věcmi (kouzla, ďábel)
- postavy: nevěrná žena, zklamaný manžel, kněz
  - postavy vnímány spíše negativně
- autoři: Jean de Boves, Jean de Condé

---

23.11.2018

## Zvířecí román
- delší než bajka
- zvířata chovající se jako lidé
- Francie, Německo, Rakousko, i v Čechách
- drsné, vulgární - fably se zvířecími postavami (satira směřuje i ke šlchtě)

  #### Renart (postava)
  "Román o Renartovi"
  - hlavní postava lyšák
  - je lstivý, v krutém světe úspěšný

  #### Yzengrin (postava)
  - vlk, je hloupý
  - lišák ho využívá

  #### Tybert (postava)
  - kocour

---

5.11.2018

## Měštanská epika

- písně pijácké, milostné, morality a satiry
### "Carmina burana"
- soubor městských písní
- objeven 1803, pochází z 13. století z Benediktbeuren

## Vývoj středověkého dramatu
- mírná dramatizace rituálu
- __žánr tropy__
  - zpívaný dialog, anděl a žena
- __officium__
  - součástí obřadu, vystupují zde mniši, v latině
- __ludus__
  - "zlidovělé" officium
- typy her:
  - miraculum (o zazráku)
  - mysterium (hra o základní pravdě, náboženské)
  - moralita (o chování lidí v duchu středověkého žánru)

#### *"Danielus Rudus"*
- král Baltazar je na hostině
- zjeví se ruka a napíše "Mene, Tekel, Fares" Sečteno, podrtženo, zváženo) -> ruka boží zvážila královi skutky
---

6.12.2018

## Jan ze Žatce
- ### *Oráč z Čech*
  - existovala verze "Tkadleček"
  - postavy: Smrt, Oráč, Bůh
  - Oráčovi zemře žena, ten chce bojovat se Smrtí, postupem času ale přijde k rozumu, chce se zbavit žalu. Rozsoudit je musí Bůh, uzná pravdu Smrti, Oráč ale sklidí čest
  - Smrt je považována za nejdůležitější prvek na Zemi, obviňuje jí z braní pouze "dobrých" lidí, ty špatné nechává žít
  - stedověké myšlení -> hlavní je posmrtný život, na zemi nemá moc cenu
  - vztah se vyvinul, Smrt začala Oráče respektovat a vysvětlovat, Bůh má pochopení pro člověka   

---

7.12.2018

- __hry o třech Mariích__
  - hrávány na Velikonoce
- __příběh o Adamovi a Evě, Kainovi a Ábelovi__
- __Mirákl o Theofilovi__

#### *Mastičkářský zvonek*
- ludus
- hra o třech Mariích, 14.století
- měly části latinské i české

#### *Hra veselé Magdaleny*
- ludus o hříšné Magdaléně
- 1. část o Luciferovi, který se chce vyrovnat Bohu
- 2. část ďábel posílá hříšníky aby šířili zlo
- 3. část Marie Magdalská se zaplétá s ďábly, obrací se zpět na víru

---

12.12.2018

# Literatura českého středověku

## Písemnictvní Velké Moravy
- kníže Rastislav přijme Cyrila a Metoděje (863)
  - chtěli christianizovat obyvatele a zvýšit fungování státu a jeho životní úroveň, kulturní výchovná činnost
  - přinesli staroslověnštinu (jednoduchá)
