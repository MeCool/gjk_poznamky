#Tkáň
= skupina buněk stejné struktury a funkce
- 4 typy:
  - epitoly
  - pojiva
  - svalová tkáň
  - nervová tkáň

__Epitely__
  - kryjí povrch těla, vystýlají orgány a dutiny uvnitř těla
  - buňky těsně naléhají -> téměř žádná mezibuněčná hmota
  - např.: žlázový, krycí, smyslový, resorpční

__Pojiva__
  - z mezodermu, podpůrná funkce
  - velké mezibuněčné prostory
  - rozlišujme:
    - vazivo
    - chrupavku
    - kost
  - Funkce:
    - vyplňují prostor mezi orgány
    - opora tělegalobalují orgán, nositelé imunity

### 1) Vazivo
- tvoří ho fibroplasty (vláknité a amorfní hmoty) + rosolovitá amorfní hmota (kolagen...)

### 2) Chrupavka
- pevná a pružná tkáň
- mezibuněčná hmota + buňky
- elastická

### 3) Kost
- tvořena buňkami (osteoplasty, osteocyty, osteoklasty) a buněčná stěna

## Trofická pojiva
- "tekutá pojiva"
- krev, míza, tkáňový mok
- Funkce:
  - výměna látek, plynů
  - nositelé imunity

## Svalová tkáň
a) příčně pruhovaná
  - umožnují pohyb organizmu
  - ovlivňujeme ji, unavuje se

b) hladká
  - tvoří vnitřní orgány
  -  nemůžeme ji ovlivnit, není unavitelná

c) srdeční
  - svalovina v srdci
  - neuvládáme ji, neunaví se

## Nervová tkáň
- buňky neurony
- dendridy = výběžek neuronu
- neurit = dlouhé výběžky
- synapse = zakončení neuronu

## Pokožka
- nebuněčná vrstva tvořená bílkovinami
- obshauje chitin, uhličitan vápenatý
- pokožka -> škára -> podkožní vazivo

### Pokožkové deriváty
- vlasy, chlupy
- kopyta, drápy, rohy
- __Kožní žlázy__
  - jedové žlázy, mazové žlázy, mléčné, potní

### Škárové deriváty
- šupiny, krunýře u želv

19.11.2018

# Opěrná a pohybová soustava:

**HYDROSTATICKÁ kostra** | **VNĚJŠÍ kostra** | **VNITŘNÍ kostra**
------|------|------
uvnitř je tekutina, která není ztlačitelná | exoskelet, stále roste, přidávají se vrstvy | endoskelet - porifera, chordata (chrupavky a kosti)
většina žahavců (žížala) | měkkýši | paryby, obratlovci
výhodná u vodních organismů | dokonalá ochrana celého těla | roste s tělem
nevhodné pro pohyb na souši | neroste se živočichem | slabší krycí schopnosti
-- | energeticky náročné | --
-- | při svlékání zranitelný | --

## Hydrostatická kostra
- tělo drží pokožka (např. žížala)
  - vnější kostra (exoskelet)
  - blokuje růst; tvořena chitinem, zbytek proteiny
  - u měkkýšů skelet stále roste, přidává se na stávající vrstvy
  - u arthropod je nutné svlékání
- vnitřní kostra (endoskelet)
  -výztuha zevnitř; chrupavky, kosti
  - především obratlovci, ale i bezobratlí (ochrana mozku chobotnice, sépiová kost, korálnatci)
  - paryby mají chrupavčitou kostru
- Typy pohybu
pomocí svalů, bičíků, brv, indulující membrány



21.11.2018

## Svaly

- je složen ze svalových vláken
  - tzv. myofibrily (= vlákno, skládající se z bílkovin)
- bílkoviny aktin a myozin (= aktinomyozinový komplex)
- na aktinová vlákna jsou navázána tropomin a tropomyozin
- vápník reguluje kontrakce tropominu -> zabraňuje neustálému stahování
- nervosvalová ploténka

26.11.2018

# Trávicí soustava:

Funkce:
- příjímání potravy (-> její zpracování)
- odstraňování nepotřebných zbytků


1. Nitrotělní
  1. Intracelulární (nitrobuněčné)
    - primitivnější typ trávení
    - buňka, která rozloží potravu ji i využije
    - prvoci, mořské houby
  2. Extracelulární (mimobuněčné)
    - probíhá v trávicí trubici (možnost polykání velkých soust)
    - natrávené částečky jsou dále tráveny intracelulárně (ve střevech)

2. Mimotělní
  - napři. pavouci (zabalení do pavučiny)


- __Trávicí dutina__ (jeden otvor na příjímání i vylučování = _gastrovaskulární systém_)
  - výhoda = je jednodušší, nepotřebuje distribuovat živiny (nepotřebuje krev)
  - nevýhoda = potrava je přijmuta, musí se zpracovat, pak vyloučena, pak až může přijmout další potravu
- __Trávicí trubice__
  - otvor pro příjem + otvor pro vylučování
  - prodlužování, specializace
  - výhody = trávení probíhá kontinuálně (různá potrava je v různých fázích trávení)
  - nevýhoda = potřebuje rozvodovou soustavu (krev), spotřebuje více energie


- Rozdělení živočichů
  - masožravci(karnivor), bíložravci(herbivor), všežravci(omnivor)
  - filtrátoři
  - přežvýkavci mají více žaludků, celulóza je težší na trávení


- *Ústa*
  - mechanické i chemické trávení (zuby + sliny)
- *Hltan*
  - posun sousta (polykání)
- *Žaludek*
  - chemický rozklad, ale i mechanický (kručení)
- *Tenké střevo*
  - vstřebávání živin
- *Tlusté střevo*
  - odvodňování, shromažďování zbytků

# Oběhová soustava
        imunita, termoregulace,
        udržování stálého vnitřního prostředí

typ oběhu:
  - rozvod tekutin bez oběhového systému

typ oběhové soustavy:
  - uzavřená cévní soustava
    - výkonnější (lepší pro větší zvířata)
  - otevřená cévní soustava (tekutina není celou dobu uzavřena v cévách)
    - sbírá se v cévách, rozlévá se do těla, vrací se zpět do cév
    - výhoda méně náročný na energii
    - nevýhoda težší distribuce do všech částí těla (tekutina je ař 1/3 jejich těla)
__oběhové tekutiny__
otevřená soustava
- hydrolymfa
- hemolymfa
uzavřená soustava
- krev (krvomíza)

## Obratlovci
- mají uzavřený systém zvaný kardiovaskulární
- srdce je tvořené předsíní (1 nebo 2) a síní (1 nebo 2)
- typy cév:
  - tepny (okysličená krev)
  - žíly (neokysličená krev)
  - kapiláry (vlásečnice)
### Ryby
- srdce má jednu predsíň a jednu síň
### Obojživelníci
- dvě předsíně a jednu komoru
- oběhy plicní a systematický (tělní)
- k míchání krve s kyslíkem a bez kyslíku v jediné komoře
### Plazi
- dvě předsíně a jedna komora
- dvojí oběh
### Ptáci a savci
- stejný krevní oběh
- dvě předsíně a dvě komory
- zcela oddělen plicní a systematický systém

# Dýchací soustava

respirační povrch = povrch, skrzte který probíhá zisk kyslíku a odstraňování
oxidu uhličitého
- žížala dýchá pomocí difuze
  - = přímo úměrná ploše respiračního povrchu a nepřímo uměrná druhé mocnině
  vzdálenosti skrze kterou musí molekuly putovat
  - difuze je možná pouze díky velice tenkému povrchu těla
- i u obojživelníků (žáby...)

- u většiny organismů není dostatek povrchu (-> plíce, tracheje...)
- __žábry__
  - jsou po celém těle, ryby zajišťují větší přísun kyslíku svým vlastním pohybem a vířením vody
- __plíce__
  - u obojživelníku jsou velice jednoduché -> kombinují plíce a povrchové přijímání kyslíku
  - plíce plazů už jsou členěné, nepotřebují kožní dýchání (mají suchý povrch)
  - ptáci mají mnohem efektivnější plíce (potřebují hodně kyslíku k létání) -> mají vzušné vaky, které posílají vzduch do plic i při výdechu
