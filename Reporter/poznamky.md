# Historie

Vychází od září 2014. Redakci tvoří převážně tým bývalých investigativních novinářů, kteří odešli z Mladé fronty Dnes po akvizici Babišovým Agrofertem. Rozsah se pohybuje mezi 130 až 150 stranami. V 60. letech již v ČSR Reportér existoval. Ten byl ovšem po normalizaci v roce 1969 z politických důvodů pozastaven. Nový Reportér sice na starý magazín nenavazuje, spíše tím vzdává hold této generaci novinářů. Magazín se při vzniku inspiroval například americkými The New Yorker nebo The Atlanticm které se vyznačují delšími, propracovanějšími články.

# Analýza tvaru (rubriky)[EDA/MIKUL]

Rubriky
Názory
(Glosy)
Report
České počiny
Střední Evropa
Byznys
Rozhovor
Kultura

Na začátku se vždy objevuje editoriál šéfredaktora Roberta Čásenského. Následuje série názorů - krátké články (1-2 A4). Texty s osobním pohledem na témata týkající se politiky na domácí scéně, problémů v zahraničí (opět politika/války…). Většinou jsou odlehčené o výraznější prvky nadsázky, autor více projevuje svůj styl (listopad - Čau vládo…).

Po názorech následují glosy, půlstránkové fejetony, které píší redaktoři, ale i nezávislí spisovatelé.

Report - reportáže (politika, ekonomika, sociální sféra)

Česko počiny obsahují články o zajímavých Češích, kteří udělali něco záslužného či inovativního (firmy či lidé, kteří se proslavili celosvětově).

V sekci Byznys nalezneme zajímavé české firmy, přinašející inovace, spolupracují se světovými značkami.

Střední Evropa - příběhy a zprávy, které zasáhly naše sousedy (příběh polské fotografky, kariéra Angely Merkelové)

Kultura - umělci, literatura apod.

# Filosofie [EDA]

Hlavním cílem je svoboda a nezávislost. Na politických subjektech, vlastnících velkých českých médií. Cílem je říkat pravdu, objektivně, s co nejmenším zatížením vlastními předsudky a preferencemi. Časopis se snaží nepřiklánět k žádné z politických orientací, ať už levici či pravici. Blíž ale pohled k pravici. Jsou pro západně orientovaní (součást EU, NATO).
Hlavní orientace co se týče obsahu jsou reportáže (investigativní). Jedná se o plnoformátový časopis (od všeho něco) - reportážně-investigativní měsíčník.
Cílová skupina jsou vzdělanější lidé (větši rozhled), lidé, kteří čtou delší texty. Převaha čtenářů ve městech. Nejvíce muži kolem 30 let (60% Muži). Skupina vyplynula přirozeně, nejedná se o cílení na přesně tento typ lidí.

# Stylovost 3 rubrik [MIKUL]

-  **Velký rozhovor s jednou osobností**

Většinou je osobnost i na titulní straně. Při pohledu na celý časopis je tento rozhovor hlavním prvkem.
Sleduje jak známé osobnosti, tak i osobu se silným / zajímavým příběhem.
Ve valné většině pojmuto formou velkého rozhovoru s otevřenými otázkami.
Otázky neočekávají jasnou odpověd ANO/NE.
Osobnosti je věnováno i 7 stránek, z čehož 2 bývají pouze fotografie spojené s osobností.
Čtenář je na počátku uveden do "děje", poté otázky směřují hlavně k aktuálním událostem spojeným s osobou.
Občas se přidá i malý sloupek, který popisuje danou osobnost další postavou, například jeho kolegou.
Jazykem se přizpůsobí dané osobnosti. S teenagerem tedy budou mluvit volněji, s politikem vážněji.


- **Historické příběhy / fakta / události**

Některé stránky se věnují událostem z minulosti.
Nebývají psány formou rozhovoru, spíše je to popis/vyprávění.
Většinou tam účinkuje nějaká postava z dnešní doby.
Může to tedy být aktuální příběh o tom, jak někdo přišel na zajímavá fakta z minulosti.
Může tak popisovat / připomínat skoro každou historickou událost.
Volba témata je zde už výrazně volnější.
Reportér si může vybrat, v době psaní článku, hodně diskutovnané téma, nebo může "vytáhnout" už zapomenutá fakta.
Stylově je to tedy souvislý vyprávěný text. Styl (spisovnost/volnost) závisí na reportérovi.
Bývá vždy tak na 2-4 stránky. Může být přiložena fotka, pokud je to vhodné
Umístěn bývá v druhé půlce magazínu.

- **Aktuální politické dění**

Bývá jako jedna z prvních rubrik. Nepřináší však jednoduchý popis, co se v politice stalo.
Poté, co čtenáře uvede do aktuálního problému, se snaží čtenářovi otevřít další úhel pohledu.
Vzhledem k faktu, že většina reportérů jsou investigativci, tak se zabývá hlavně politickými prohřešky a přešlapy.
Protože je magazín měsíční, má možnost si vybrat události opravdu podstatné nebo dostatečně zajímavé.
Stylově se tedy bavíme o textu na 1-3 stránky. Reportér je vždy velmi znalý v celkovém dění.
Často je úvod rubriky psán souvislým textem popisného charakteru. Později může být text doplněn rozhovorem, fotkami, grafy - tedy jakýmkoli doprovodným materiálem souvisejícím s tématem.



# Hledat známé novináře (pravidelné)[EDA]

Výrazná osobnost - Jaroslav Kmenta
Jaroslav Kmenta patří ke špičce české investigativní novinařiny. Zaměřuje se na dění v české politické scéně spojené s korupcí a propojení s podnikatelskými a mafiánskými kruhy. Podílel se na rozkývání takových kauz, jako například kauza kmotra Mrázka (doplnit), zločineckého podnikatele Radovana Krejčího či podvodům současného premiéra Andreje Babiše. Na podkladech jeho práce byly natočeny i dva filmy, Příběh Kmotra a Gangster Ka. Publikoval i několik knih, mezi nimi například Kmotr Mrázek, Padrino Krejčíř nebo Boss Babiš.

" Proč je dobré mít v redakci Jaroslava Kmentu? Protože ho nikdo nemá rád … Tedy přesněji, nemá ho ráda drtivá většina politiků, ať zprava nebo zleva. To může být důkazem, že svou práci investigativního reportéra dělal dobře a navíc nestranně.	“         — Robert Čásenský, šéfredaktor MF DNES
	- Třídy	

# Design časopisu [MIKUL]

Uniformní design, černo-oranžové schéma. Patkový font. Text je uspořádán do tří sloupců, zároveň je proložený fotografiemi, obecně 1:3 (fotky:text). Do textu jsou vloženy vyjmuté věty, psané kapitálkami, podtržené oranžovou čarou. Jedná se o klíčové, šokující informace obsažené v textu, nutící čtenáře pokračovat ve čtení.
Magazín se drží poměrně minimalistického designu. Soustředí se na osovou souměrnost.

Titulní strana také není křiklavá, drží se minimalistické myšlenky časopisu, zároveň se stále dokáže "prodat"

Reklama je výrazná a častá. Vzhledem k tomu, že je financována z prodejního zisku a ze soukromých zdrojů, tak je reklama aspoň částěčně pochopitelná, i když osobně bych řekl, že už jí je věnováno příliš prostoru. Občas se objeví i na tvrzené stránce, což zapříčiní, že při rychlém listování se na reklamě vždy zastavíte.

Používají kvalitní fotografie na vyšší úrovni - focené pravděpodobně vlastním fotografem 
